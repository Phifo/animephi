class Series < ActiveRecord::Base
  has_many :episodes
  attr_accessible :synopsis, :title, :image
  
  if Rails.env != "development"
    has_attached_file :image,
    :styles => { :medium => "282x450", :thumb => "260x180#" },
    :storage => :dropbox,
    :dropbox_credentials => "#{Rails.root}/config/dropbox.yml",
    :dropbox_options => {
      :unique_filename => true
    }
  else
    has_attached_file :image,
    :styles => { :medium => "282x450", :thumb => "260x180#" }
  end
end
