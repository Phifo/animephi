class Episode < ActiveRecord::Base
  belongs_to :series
  attr_accessible :number, :series_id, :source, :title
end
