class HomeController < ApplicationController
  before_filter :authenticate_user!

  def index
    @series = Series.order("title ASC")
  end

  def show
    @serie = Series.find(params[:id])
    @episodes = @serie.episodes.order("number ASC")
  end

  def episode
    @episode = Episode.find(params[:id])

    ant = @episode.number - 1
    sig = @episode.number + 1
    @next = Episode.find_by_number_and_series_id(sig, @episode.series.id)
    @pre = Episode.find_by_number_and_series_id(ant, @episode.series.id)
  end
end
