class UsersController < ApplicationController
  before_filter :authenticate_admin

  def index
    if params[:approved] == "false"
      @users = User.find_all_by_approved(false)
    else
      @users = User.all
    end
  end

  def approve
    @user = User.find(params[:id])

    @user.approved = true

    respond_to do |format|
      if @user.update_attributes(params[:user])
        AdminMailer.new_user_approved(@user).deliver
        format.html { redirect_to users_path, notice: 'User was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
end
