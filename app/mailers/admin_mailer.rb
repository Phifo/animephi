class AdminMailer < ActionMailer::Base
  default from: "admin@phifo.cl"

  def new_user_waiting_for_approval(user)
    @user = user
    mail(:to => "fernandez.chl@gmail.com", :subject => "New registration PHIANIME")
  end

  def new_user_approved(user)
    @user = user
    mail(:to => @user.email, :subject => "Solicitud aprobada! de anime.phifo.cl")
  end
end
