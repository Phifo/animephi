class AddImageColumnsToSeries < ActiveRecord::Migration
  def up
    add_attachment :series, :image
  end

  def down
    remove_attachment :series, :image
  end
end
