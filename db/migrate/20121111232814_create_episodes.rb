class CreateEpisodes < ActiveRecord::Migration
  def change
    create_table :episodes do |t|
      t.integer :number
      t.string :title
      t.string :source
      t.integer :series_id

      t.timestamps
    end
  end
end
